package fr.pomodoro.service;

import fr.pomodoro.entity.History;
import fr.pomodoro.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class HistoryService {

    private final HistoryRepository historyRepository;

    @Autowired
    public HistoryService(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    /**
     * Fonction pour récupérer un objet history par ID.
     * @param id
     * @return History
     */
    public History getHistoryById(UUID id) {
        return historyRepository.findById(id).orElse(null);
    }

    /**
     * Fonction pour récupérer tous les objets history
     * @return List<History>
     */
    public List<History> getHistoryList() {
        return historyRepository.findAllByOrderByDateDesc();
    }

    /**
     * Procédure pour créer un objet history
     * @param action
     * @param break_length
     * @param session_length
     * @param time
     * @param type
     */
    public History addHistory(String action, String break_length, String session_length, String time, String type) {
        History history = new History(action, break_length, session_length, time, type);
        return historyRepository.save(history);
    }

    /**
     * Procédure pour supprimer un objet history par ID
     * @param id
     */
    public void deleteHistory(UUID id) {
        historyRepository.deleteById(id);
    }
}
