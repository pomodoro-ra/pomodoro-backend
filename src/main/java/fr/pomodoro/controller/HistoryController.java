package fr.pomodoro.controller;

import fr.pomodoro.dto.HistoryRequest;
import fr.pomodoro.entity.History;
import fr.pomodoro.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*")
@RestController
public class HistoryController {

    private final HistoryService historyService;

    @Autowired
    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    /**
     * Endpoint API pour récupérer un objet history par ID
     * @param id
     * @return History
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/api/history/{id}")
    public History getHistory(@PathVariable UUID id) {
        return historyService.getHistoryById(id);
    }

    /**
     * Endpoint API pour récupérer tous les objets history
     * @return List<History>
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/api/history/list")
    public List<History> getHistoryList() {
        return historyService.getHistoryList();
    }

    /**
     * @param request
     * @return
     */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/api/history")
    public History createHistory(@RequestBody HistoryRequest request) {
        return historyService.addHistory(
                request.getAction(),
                request.getBreakLength(),
                request.getSessionLength(),
                request.getTime(),
                request.getType());
    }

    /**
     * Endpoint API pour supprimer un objet history par ID
     * @param id
     */
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/api/history/{id}")
    public void deleteHistory(@PathVariable UUID id) {
        historyService.deleteHistory(id);
    }

}
