package fr.pomodoro.entity;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.UUID;


@Entity
@Table(name = "HISTORY")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "ID", unique = true)
    private UUID id;

    @Column(name = "DATE")
    private LocalDateTime date;

    @Column(name = "ACTION")
    private String action;

    @Column(name = "BREAK_LENGTH")
    private String break_length;

    @Column(name = "SESSION_LENGTH")
    private String session_length;

    @Column(name = "TIME")
    private String time;

    @Column(name = "TYPE")
    private String type;

    public History() {

    }

    public History(String action, String break_length, String session_length, String time, String type) {
        this.date = LocalDateTime.now();
        this.action = action;
        this.break_length = break_length;
        this.session_length = session_length;
        this.time = time;
        this.type = type;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getBreak_length() {
        return break_length;
    }

    public void setBreak_length(String break_length) {
        this.break_length = break_length;
    }

    public String getSession_length() {
        return session_length;
    }

    public void setSession_length(String session_length) {
        this.session_length = session_length;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
