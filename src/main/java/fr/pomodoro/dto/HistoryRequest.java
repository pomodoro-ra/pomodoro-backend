package fr.pomodoro.dto;

public class HistoryRequest {
    private String action;
    private String breakLength;
    private String sessionLength;
    private String time;
    private String type;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getBreakLength() {
        return breakLength;
    }

    public void setBreakLength(String breakLength) {
        this.breakLength = breakLength;
    }

    public String getSessionLength() {
        return sessionLength;
    }

    public void setSessionLength(String sessionLength) {
        this.sessionLength = sessionLength;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}