package fr.pomodoro.service;

import fr.pomodoro.entity.History;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.sql.Array;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ActiveProfiles("test")
class HistoryServiceTest {

    @Autowired
    private HistoryService historyService;

    private final String action = "testAction";
    private final String breakLength = "testBreakLength";
    private final String sessionLength = "testSessionLength";
    private final String time = "testTime";
    private final String type = "testType";

    private static UUID id;

    /**
     * Test pour la création d'un objet history a partir du service
     */
    @Test
    @Order(1)
    void addHistory() {
        History historyCreated = historyService.addHistory(this.action, this.breakLength, this.sessionLength, this.time, this.type);
        id = historyCreated.getId();

        assertEquals(this.action, historyCreated.getAction());
        assertEquals(this.breakLength, historyCreated.getBreak_length());
        assertEquals(this.sessionLength, historyCreated.getSession_length());
        assertEquals(this.time, historyCreated.getTime());
        assertEquals(this.type, historyCreated.getType());
    }

    @Test
    @Order(2)
    void getHistoryById() {
        History historyGetById = historyService.getHistoryById(id);

        assertNotEquals(null, historyGetById);

        assertEquals(id, historyGetById.getId());
        assertEquals(this.action, historyGetById.getAction());
        assertEquals(this.breakLength, historyGetById.getBreak_length());
        assertEquals(this.sessionLength, historyGetById.getSession_length());
        assertEquals(this.time, historyGetById.getTime());
        assertEquals(this.type, historyGetById.getType());
    }

    @Test
    @Order(3)
    void getHistoryList() {
        List<History> histories = historyService.getHistoryList();

        assertNotEquals(null, histories);
        assertEquals(1, histories.size());

        History history = histories.get(0);

        assertEquals(id, history.getId());
        assertEquals(this.action, history.getAction());
        assertEquals(this.breakLength, history.getBreak_length());
        assertEquals(this.sessionLength, history.getSession_length());
        assertEquals(this.time, history.getTime());
        assertEquals(this.type, history.getType());
    }

    @Test
    @Order(4)
    void deleteHistory() {
        historyService.deleteHistory(id);

        List<History> histories = historyService.getHistoryList();
        assertEquals(0, histories.size());

        History history = historyService.getHistoryById(id);
        assertNull(history);
    }
}
