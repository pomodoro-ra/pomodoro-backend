package fr.pomodoro.controller;
import fr.pomodoro.dto.HistoryRequest;
import fr.pomodoro.entity.History;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,  properties = {"server.port=8080"})
@AutoConfigureTestDatabase
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ActiveProfiles("test")
class HistoryControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private static History history = new History();


    @Test
    @Order(1)
    void createHistory() {
        // Création d'un objet HistoryRequest avec les données appropriées
        HistoryRequest request = new HistoryRequest();
        request.setAction("some_action");
        request.setBreakLength("10");
        request.setSessionLength("25");
        request.setTime("2022-06-03T12:00:00");
        request.setType("some_type");

        // Construction de l'URL
        String url = "http://localhost:8080/api/history";

        // Création de l'entité de requête avec l'objet HistoryRequest
        HttpEntity<HistoryRequest> requestEntity = new HttpEntity<>(request);

        // Envoi de la requête POST à l'endpoint
        ResponseEntity<History> response = restTemplate.exchange(
                url,
                HttpMethod.POST,
                requestEntity,
                History.class);

        // Vérification du code de statut de la réponse
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        History tempHistory = response.getBody();
        assertNotNull(tempHistory);
        history.setId(tempHistory.getId());
        history.setAction(tempHistory.getAction());
        history.setDate(tempHistory.getDate());
        history.setTime(tempHistory.getTime());
        history.setBreak_length(tempHistory.getBreak_length());
        history.setSession_length(tempHistory.getSession_length());
        history.setType(tempHistory.getType());
    }

    @Test
    @Order(2)
    void getHistory() {
        // Construct URL
        String url = "http://localhost:8080/api/history/";
        System.out.println(history.getId());

        // Create request entity with parameters
        HttpEntity<String> requestEntity = new HttpEntity<>(null);

        // Send GET request to the endpoint
        ResponseEntity<History> response = restTemplate.exchange(
                url + history.getId(),
                HttpMethod.GET,
                requestEntity,
                History.class);

        // Verify response status
        assertEquals(HttpStatus.OK, response.getStatusCode());
        History resultHistory = response.getBody();
        assertNotNull(resultHistory);
        assertEquals(resultHistory.getId(), history.getId());
        assertEquals(resultHistory.getAction(), history.getAction());
        assertEquals(resultHistory.getBreak_length(), history.getBreak_length());
        assertEquals(resultHistory.getSession_length(), history.getSession_length());
        assertEquals(resultHistory.getTime(), history.getTime());
        assertEquals(resultHistory.getType(), history.getType());
    }

    @Test
    @Order(3)
    void getHistoryList() {
        // Construct URL
        String url = "http://localhost:8080/api/history/list";

        // Create request entity with parameters
        HttpEntity<String> requestEntity = new HttpEntity<>(null);

        // Send GET request to the endpoint
        ResponseEntity<List> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                requestEntity,
                List.class);

        // Verify response status
        System.out.println(Objects.requireNonNull(response.getBody()).size());
        assertEquals(1, response.getBody().size());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @Order(4)
    void deleteHistory() {
        // Construct URL
        String url = "http://localhost:8080/api/history/";

        // Create request entity with parameters
        HttpEntity<String> requestEntity = new HttpEntity<>(null);

        // Send GET request to the endpoint
        ResponseEntity<String> response = restTemplate.exchange(
                url + history.getId(),
                HttpMethod.DELETE,
                requestEntity,
                String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}