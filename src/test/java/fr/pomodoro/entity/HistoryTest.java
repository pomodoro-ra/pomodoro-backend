package fr.pomodoro.entity;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class HistoryTest {

    private static final LocalDateTime date = LocalDateTime.now();
    private static final UUID uuid = UUID.randomUUID();
    @Test
    void setId() {
        History history = new History();
        history.setId(uuid);
        assertEquals(uuid, history.getId());
    }

    @Test
    void getId() {
        History history = new History();
        history.setId(uuid);
        assertEquals(uuid, history.getId());
    }

    @Test
    void getDate() {
        History history = new History();
        history.setDate(date);
        assertEquals(date, history.getDate());
    }

    @Test
    void setDate() {
        History history = new History();
        history.setDate(date);
        assertEquals(date, history.getDate());
    }

    @Test
    void getAction() {
        History history = new History();
        history.setAction("someAction");
        assertEquals("someAction", history.getAction());
    }

    @Test
    void setAction() {
        History history = new History();
        history.setAction("someAction");
        assertEquals("someAction", history.getAction());
    }

    @Test
    void getBreak_length() {
        History history = new History();
        history.setBreak_length("10");
        assertEquals("10", history.getBreak_length());
    }

    @Test
    void setBreak_length() {
        History history = new History();
        history.setBreak_length("10");
        assertEquals("10", history.getBreak_length());
    }

    @Test
    void getSession_length() {
        History history = new History();
        history.setSession_length("25");
        assertEquals("25", history.getSession_length());
    }

    @Test
    void setSession_length() {
        History history = new History();
        history.setSession_length("25");
        assertEquals("25", history.getSession_length());
    }

    @Test
    void getTime() {
        History history = new History();
        history.setTime("12:00");
        assertEquals("12:00", history.getTime());
    }

    @Test
    void setTime() {
        History history = new History();
        history.setTime("12:00");
        assertEquals("12:00", history.getTime());
    }

    @Test
    void getType() {
        History history = new History();
        history.setType("SomeType");
        assertEquals("SomeType", history.getType());
    }

    @Test
    void setType() {
        History history = new History();
        history.setType("SomeType");
        assertEquals("SomeType", history.getType());
    }
}
