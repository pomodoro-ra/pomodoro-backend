package fr.pomodoro.dto;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class HistoryRequestTest {

    @Test
    void getAction() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setAction("someAction");
        assertEquals("someAction", historyRequest.getAction());
    }

    @Test
    void setAction() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setAction("someAction");
        assertEquals("someAction", historyRequest.getAction());
    }

    @Test
    void getBreakLength() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setBreakLength("10");
        assertEquals("10", historyRequest.getBreakLength());
    }

    @Test
    void setBreakLength() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setBreakLength("10");
        assertEquals("10", historyRequest.getBreakLength());
    }

    @Test
    void getSessionLength() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setSessionLength("25");
        assertEquals("25", historyRequest.getSessionLength());
    }

    @Test
    void setSessionLength() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setSessionLength("25");
        assertEquals("25", historyRequest.getSessionLength());
    }

    @Test
    void getTime() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setTime("12:00");
        assertEquals("12:00", historyRequest.getTime());
    }

    @Test
    void setTime() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setTime("12:00");
        assertEquals("12:00", historyRequest.getTime());
    }

    @Test
    void getType() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setType("SomeType");
        assertEquals("SomeType", historyRequest.getType());
    }

    @Test
    void setType() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setType("SomeType");
        assertEquals("SomeType", historyRequest.getType());
    }
}
