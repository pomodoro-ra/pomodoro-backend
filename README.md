### Install

Install java jdk-17
Open project and reload maven dependencies
Update application.properties with gitlab ci variables
Start poject

### Schema

https://drive.google.com/file/d/1q-831YmJDxpOUWgnR99D9xhWxwSULT3n/view?usp=sharing


### Endpoint

@GetMapping("http://152.228.171.197/api/history/{id}")
Obtenir un historique par ID

@GetMapping("http://152.228.171.197/api/history/list")
Obtenir la liste de tout l'historique

@PostMapping("http://152.228.171.197/api/history")
Créer un historique : @RequestBody HistoryRequest request
ex application/json: {
"action": "some_action",
"breakLength": "10",
"sessionLength": "25",
"time": "2022-06-03T12:00:00",
"type": "some_type"
}

@DeleteMapping("http://152.228.171.197/api/history/{id}")
Supprimer un historique par ID
